package com.trb.bdi.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Alias("ui")
@Data
public class UserInfoVO {
	private Integer uiNum;
	private String uiName;
	private String uiId;
	private String uiPwd;
	private Integer credat;
	private Integer cretim;	
	private Integer moddat;
	private Integer modtim;	
	private Integer active;	
}
