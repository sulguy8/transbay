package com.trb.bdi.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;
@Alias("vn")
@Data
public class VenueInfoVO {
	private String vnNum;
	private String vnName;
	private String vnImg;
	private String vnCtry;
	private String vnAddr;
	private Integer vnLike;
}
