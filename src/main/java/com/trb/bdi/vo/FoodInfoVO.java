package com.trb.bdi.vo;

import java.util.List;

import org.apache.ibatis.type.Alias;

import lombok.Data;
@Alias("fi")
@Data
public class FoodInfoVO {

	private String fdNum;
	private String fdName;
	private String fdInfo;
	private String fdImg;
	private String fdCountry;
	private Integer fdCnt;
	private Integer fdCstm;
	private Integer fdStore;
	private Integer fdLike;
	
	private String id;
	private List<FoodInfoVO> result;
}
