package com.trb.bdi.vo.ppgVO;

import lombok.Data;

@Data
public class Message {
	private Result result;
}
