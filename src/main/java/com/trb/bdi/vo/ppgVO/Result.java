package com.trb.bdi.vo.ppgVO;

import lombok.Data;

@Data
public class Result {
	
	private String srcLangType;
	private String tarLangType;
	private String translatedText;
}
