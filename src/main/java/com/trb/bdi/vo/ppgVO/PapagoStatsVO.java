package com.trb.bdi.vo.ppgVO;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Alias("ps")
@Data
public class PapagoStatsVO {
	private Integer psNum;
	private Integer uiNum;
	private Integer piNum;
	private String credat;
	private String order;
	private String uiId;
	private String cnt;
}
