package com.trb.bdi.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trb.bdi.service.PapagoService;
import com.trb.bdi.service.PapagoStatsService;
import com.trb.bdi.vo.UserInfoVO;
import com.trb.bdi.vo.ppgVO.Message;
import com.trb.bdi.vo.ppgVO.PapagoStatsVO;
import com.trb.bdi.vo.ppgVO.TransVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class PapagoController {
	
	@Resource
	private PapagoService pService;
	
	@Resource
	private PapagoStatsService psService;
	
	@GetMapping("/papago")
	public List<PapagoStatsVO> getPapagoStatsList(@ModelAttribute PapagoStatsVO ps, HttpServletRequest request) {
		String str = request.getHeader("credat");
		if("true".equals(str)) {
			return psService.selectCredatStats(ps);
		}
		return psService.selectPapagoStats(ps);
	}
	
	@PostMapping("/papago")
	public Message doTranslate(@ModelAttribute TransVO trs, HttpSession hs) {
		UserInfoVO ui = (UserInfoVO)hs.getAttribute("ui");
		trs.setUiNum(101);
		return pService.doTranslate(trs);
	}
}
