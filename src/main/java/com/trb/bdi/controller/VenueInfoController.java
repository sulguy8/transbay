package com.trb.bdi.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;

import com.trb.bdi.service.VenueInfoService;
import com.trb.bdi.vo.TestInfoVO;
import com.trb.bdi.vo.VenueInfoVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class VenueInfoController {
	@Resource
	private VenueInfoService viService;
	
	@GetMapping("/venue")
	public List<VenueInfoVO> getVenue(@ModelAttribute VenueInfoVO vi){
		log.info("test=>{}",vi);
		return viService.selectVenueInfo(vi);
	}
	
	@GetMapping("/venues")
	public List<VenueInfoVO> getVenueInfoList(){
		return viService.selectVenueInfoList(null);
	}
	
	
	
	// Paging test
	@GetMapping("/tests")
	public Map<String,Object> getTests(@ModelAttribute TestInfoVO test){
		return viService.selectTestInfoList(test);
	}
	
//	@PostMapping("/Venue/login")
//	public Map<String,Object> doLogin(@RequestBody VenueInfoVO Venue, HttpSession session) {
//		return uiService.doLogin(Venue, session);
//	}
//
//	@GetMapping("/Venue/logout")
//	public ModelAndView doLogout(HttpSession session){
//		session.invalidate();
//		ModelAndView mav = new ModelAndView("/app/menu");
//		mav.addObject("msg", "로그아웃 되었습니다.");
//		return mav;
//	}
//	
//	@PostMapping("/Venue")
//	public Map<String,Object> insertVenue(@RequestBody VenueInfoVO Venue){		
//		log.info("test=>{}",Venue);
//		return uiService.insertVenueInfo(Venue);
//	}
//	
//	@PutMapping("/Venue")
//	public Map<String,Object> updateVenue(@RequestBody VenueInfoVO Venue){
//		log.info("test=>{}",Venue);
//		return uiService.updateVenueInfo(Venue);
//	}
//	
//	@DeleteMapping("/Venue")
//	public Map<String,Object> deleteVenue(@RequestBody VenueInfoVO Venue){
//		return uiService.deleteVenueInfo(Venue);
//	}	
}
