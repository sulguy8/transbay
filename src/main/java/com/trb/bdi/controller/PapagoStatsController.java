package com.trb.bdi.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.trb.bdi.service.PapagoStatsService;
import com.trb.bdi.vo.ppgVO.PapagoStatsVO;

@RestController
public class PapagoStatsController {
	
	@Resource
	private PapagoStatsService psService;
	
	@GetMapping("/papago/stats")
	public List<PapagoStatsVO> getPapagoStatsList(PapagoStatsVO ps){
		return psService.selectPapagoStats(ps);
	}
}
	
