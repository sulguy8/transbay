package com.trb.bdi.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RestController;

import com.trb.bdi.service.FoodInfoService;
import com.trb.bdi.vo.FoodInfoVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class FoodController {
	@Resource
	private FoodInfoService fi;
	
	@GetMapping("/onkey/**")
	public FoodInfoVO doTranslate(@ModelAttribute FoodInfoVO fd) { 
		FoodInfoVO result = new FoodInfoVO();
		result.setResult(fi.selectOnkeyList(fd));
		return result;
	}
	
	@GetMapping("/food")
	public List<FoodInfoVO> getFood(@ModelAttribute FoodInfoVO fd){

		return fi.selectFoodInfo(fd);
	}
//	
//	@GetMapping("/foods")
//	public List<FoodInfoVO> getFoodInfoList(){
//		return fi.selectUserInfoList(null);
//	}
//	
//	@PostMapping("/food")
//	public Map<String,Object> insertFood(@RequestBody UserInfoVO user){		
//		log.info("test=>{}",user);
//		return fi.insertUserInfo(user);
//	}
//	
//	@PutMapping("/food")
//	public Map<String,Object> updateFood(@RequestBody UserInfoVO user){
//		log.info("test=>{}",user);
//		return fi.updateUserInfo(user);
//	}
//	
//	@DeleteMapping("/food")
//	public Map<String,Object> deleteFood(@RequestBody UserInfoVO user){
//		return fi.deleteUserInfo(user);
//	}	
	
}
