package com.trb.bdi.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.trb.bdi.service.UserInfoService;
import com.trb.bdi.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UserInfoController {
	@Resource
	private UserInfoService uiService;
	
	@GetMapping("/user")
	public List<UserInfoVO> getUser(@ModelAttribute UserInfoVO ui){
		log.info("test=>{}",ui);
		return uiService.selectUserInfo(ui);
	}
	
	@GetMapping("/users")
	public List<UserInfoVO> getUserInfoList(){
		return uiService.selectUserInfoList(null);
	}

	@PostMapping("/user")
	public Map<String,Object> insertUser(@RequestBody UserInfoVO ui){		
		log.info("test=>{}",ui);
		return uiService.insertUserInfo(ui);
	}
	
	@PutMapping("/user")
	public Map<String,Object> updateUser(@RequestBody UserInfoVO ui){
		log.info("test=>{}",ui);
		return uiService.updateUserInfo(ui);
	}
	
	@DeleteMapping("/user")
	public Map<String,Object> deleteUser(@RequestBody UserInfoVO ui){
		return uiService.deleteUserInfo(ui);
	}	
	
	@PostMapping("/user/login")
	public Map<String,Object> doLogin(@RequestBody UserInfoVO ui, HttpSession session) {
		return uiService.doLogin(ui, session);
	}

	@GetMapping("/user/logout")
	public ModelAndView doLogout(HttpSession session){
		session.invalidate();
		ModelAndView mav = new ModelAndView("/index");
		mav.addObject("msg", "로그아웃 되었습니다.");
		return mav;
	}
}
