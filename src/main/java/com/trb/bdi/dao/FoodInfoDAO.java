package com.trb.bdi.dao;

import java.util.List;

import com.trb.bdi.vo.FoodInfoVO;

public interface FoodInfoDAO {
	public List<FoodInfoVO> selectFoodInfo(FoodInfoVO fd);
	public List<FoodInfoVO> selectOnKeyList(FoodInfoVO fd);
}
