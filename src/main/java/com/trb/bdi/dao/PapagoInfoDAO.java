package com.trb.bdi.dao;

import java.util.List;

import com.trb.bdi.vo.ppgVO.PapagoInfoVO;

public interface PapagoInfoDAO {
	public List<PapagoInfoVO> selectPapagoList(PapagoInfoVO pi);
	public List<PapagoInfoVO> selectPapagoVOList(PapagoInfoVO pi);
	public int insertPapagoInfo(PapagoInfoVO pi);
	public int updatePapagoInfo(PapagoInfoVO pi);
	public int deletePapagoInfo(PapagoInfoVO pi);
	public PapagoInfoVO selectPapagoInfo(PapagoInfoVO pi);
	public int updatePapagoInfoCnt(PapagoInfoVO pi);
}
