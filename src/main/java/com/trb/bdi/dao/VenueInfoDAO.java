package com.trb.bdi.dao;

import java.util.List;
import java.util.Map;

import com.trb.bdi.vo.TestInfoVO;
import com.trb.bdi.vo.VenueInfoVO;

public interface VenueInfoDAO {
	public List<VenueInfoVO> selectVenueInfo(VenueInfoVO vi);
	public List<VenueInfoVO> selectVenueInfoList(VenueInfoVO vi);
	public int insertVenueInfo(VenueInfoVO vi);
	public int updateVenueInfo(VenueInfoVO vi);
	public int deleteVenueInfo(VenueInfoVO vi);
	
	public Map<String,Object> selectTestInfoList(TestInfoVO test);
	
}
