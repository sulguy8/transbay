package com.trb.bdi.dao;

import java.util.List;

import com.trb.bdi.vo.ppgVO.PapagoStatsVO;

public interface PapagoStatsDAO {
	public List<PapagoStatsVO> selectPapagoStats(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectCredatStats(PapagoStatsVO ps);
	public int insertPapagoStats(PapagoStatsVO ps);
	
}
