package com.trb.bdi.dao;

import java.util.List;

import com.trb.bdi.vo.UserInfoVO;

public interface UserInfoDAO {
	public List<UserInfoVO> selectUserInfo(UserInfoVO ui);
	public List<UserInfoVO> selectUserInfoList(UserInfoVO ui);
	public int insertUserInfo(UserInfoVO ui);
	public int updateUserInfo(UserInfoVO ui);
	public int deleteUserInfo(UserInfoVO ui);
	
	public UserInfoVO doLogin(UserInfoVO ui);
	
}
