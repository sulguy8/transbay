package com.trb.bdi.dao.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.trb.bdi.dao.UserInfoDAO;
import com.trb.bdi.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class UserInfoDAOImpl implements UserInfoDAO {
	@Resource
	private SqlSessionFactory ssf;
	
	@Override
	public List<UserInfoVO> selectUserInfo(UserInfoVO ui) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.trb.bdi.UserInfoMapper.selectUserInfo",ui);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}   
		return null;
	}

	@Override
	public List<UserInfoVO> selectUserInfoList(UserInfoVO ui) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.trb.bdi.UserInfoMapper.selectUserInfoList",ui);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}

	@Override
	public int insertUserInfo(UserInfoVO ui) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.insert("com.trb.bdi.UserInfoMapper.insertUserInfo",ui);
			ss.commit();
			return cnt;
		}catch(Exception e) {
			ss.rollback();
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return 0;
	}

	@Override
	public int updateUserInfo(UserInfoVO ui) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.update("com.trb.bdi.UserInfoMapper.updateUserInfo",ui);
			ss.commit();
			return cnt;
		}catch(Exception e) {
			ss.rollback();
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return 0;
	}

	@Override
	public int deleteUserInfo(UserInfoVO ui) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.delete("com.trb.bdi.UserInfoMapper.deleteUserInfo",ui);
			ss.commit();
			return cnt;
		}catch(Exception e) {
			ss.rollback();
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return 0;
	}

	@Override
	public UserInfoVO doLogin(UserInfoVO ui) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectOne("com.trb.bdi.UserInfoMapper.doLogin",ui);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}   
		return null;
	}

}
