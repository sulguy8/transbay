package com.trb.bdi.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.trb.bdi.dao.PapagoInfoDAO;
import com.trb.bdi.vo.ppgVO.PapagoInfoVO;

@Repository
public class PapagoInfoDAOimpl implements PapagoInfoDAO {

	@Resource
	private SqlSessionFactory ssf;
	
	@Override
	public List<PapagoInfoVO> selectPapagoList(PapagoInfoVO pi) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.trb.bdi.PapagoInfoMapper.selectPapagoInfoList", pi);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.commit();
			ss.close();

		}
		return null;
	}

	@Override
	public List<PapagoInfoVO> selectPapagoVOList(PapagoInfoVO pi) {
		SqlSession ss = ssf.openSession();
		
		try {
			return ss.selectList("com.trb.bdi.PapagoInfoMapper.selectPapagoInfoList");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.commit();
			ss.close();

		}
		return null;
	}

	@Override
	public int insertPapagoInfo(PapagoInfoVO pi) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.insert("com.trb.bdi.PapagoInfoMapper.insertPapagoInfo", pi);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.commit();
			ss.close();

		}
		return 0;
	}

	@Override
	public int updatePapagoInfo(PapagoInfoVO pi) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deletePapagoInfo(PapagoInfoVO pi) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public PapagoInfoVO selectPapagoInfo(PapagoInfoVO pi) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectOne("com.trb.bdi.PapagoInfoMapper.selectPapagoInfo", pi);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.commit();
			ss.close();

		}
		return null;
	}

	@Override
	public int updatePapagoInfoCnt(PapagoInfoVO pi) {
		
		System.out.println("나는 업데이트" + pi);
		SqlSession ss = ssf.openSession();
		try {
			return ss.update("com.trb.bdi.PapagoInfoMapper.updatePapagoInfoForCnt", pi);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.commit();
			ss.close();

		}
		return 0;
	}
}
