package com.trb.bdi.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.trb.bdi.dao.VenueInfoDAO;
import com.trb.bdi.vo.PageVO;
import com.trb.bdi.vo.TestInfoVO;
import com.trb.bdi.vo.VenueInfoVO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class VenueInfoDAOImpl implements VenueInfoDAO {
	@Resource
	private SqlSessionFactory ssf;
	
	@Override
	public List<VenueInfoVO> selectVenueInfo(VenueInfoVO vi) {
		SqlSession ss = ssf.openSession();
		
		try {
			return ss.selectList("com.trb.bdi.VenueInfoMapper.selectVenueInfo", vi);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}   
		return null;
	}

	@Override
	public List<VenueInfoVO> selectVenueInfoList(VenueInfoVO vi) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.trb.bdi.VenueInfoMapper.selectVenueInfoList", vi);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}

	@Override
	public int insertVenueInfo(VenueInfoVO vi) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.insert("com.trb.bdi.VenueInfoMapper.insertVenueInfo", vi);
			ss.commit();
			return cnt;
		}catch(Exception e) {
			ss.rollback();
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return 0;
	}

	@Override
	public int updateVenueInfo(VenueInfoVO vi) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.update("com.trb.bdi.VenueInfoMapper.updateVenueInfo", vi);
			ss.commit();
			return cnt;
		}catch(Exception e) {
			ss.rollback();
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return 0;
	}

	@Override
	public int deleteVenueInfo(VenueInfoVO vi) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.delete("com.trb.bdi.VenueInfoMapper.deleteVenueInfo", vi);
			ss.commit();
			return cnt;
		}catch(Exception e) {
			ss.rollback();
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return 0;
	}

	@Override
	public Map<String, Object> selectTestInfoList(TestInfoVO test) {
		SqlSession ss = ssf.openSession();
		int startNum = test.getPage().getPage()*10-10;
		test.getPage().setStartNum(startNum);
		Integer totalCount = ss.selectOne("com.trb.bdi.VenueInfoMapper.totalTestInfoCount", test);
		System.out.println(totalCount);
		Map<String,Object> rMap = new HashMap<String,Object>();
		rMap.put("list", ss.selectList("com.trb.bdi.VenueInfoMapper.selectTestInfoList", test));
		PageVO page = test.getPage();
		page.setTotalCount(totalCount);
		rMap.put("page", page);
		return rMap;
	}
}
