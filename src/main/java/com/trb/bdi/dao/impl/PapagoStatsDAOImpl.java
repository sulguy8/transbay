package com.trb.bdi.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.trb.bdi.dao.PapagoStatsDAO;
import com.trb.bdi.vo.ppgVO.PapagoStatsVO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class PapagoStatsDAOImpl implements PapagoStatsDAO {
	
	@Resource
	private SqlSessionFactory ssf;
	
	@Override
	public List<PapagoStatsVO> selectPapagoStats(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.trb.bdi.PapagoStatsInfoMapper.selectPapagoInfoList",ps);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}
	
	@Override
	public List<PapagoStatsVO> selectCredatStats(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.trb.bdi.PapagoStatsInfoMapper.selectCredatCntList",ps);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}
	

	@Override
	public int insertPapagoStats(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.insert("com.trb.bdi.PapagoStatsInfoMapper.insertPapagoInfo",ps);
			ss.commit();
			return cnt;
		}catch(Exception e) {
			ss.rollback();
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return 0;
	}


}
