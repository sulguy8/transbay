package com.trb.bdi.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.trb.bdi.dao.FoodInfoDAO;
import com.trb.bdi.vo.FoodInfoVO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class FoodInfoDAOImpl implements FoodInfoDAO {
	
	@Resource
	private SqlSessionFactory ssf;
	
	@Override
	public List<FoodInfoVO> selectFoodInfo(FoodInfoVO fd) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.trb.bdi.FoodInfoMapper.selectFoodInfo",fd);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}
	
	@Override
	public List<FoodInfoVO> selectOnKeyList(FoodInfoVO fd) {
		FoodInfoVO ok = new FoodInfoVO();
		SqlSession ss = ssf.openSession();
		List<FoodInfoVO> result = ss.selectList("com.trb.bdi.FoodInfoMapper.selectOnkeyInfoList", fd);
		try {
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.commit();
			ss.close();
		}
		return null;
	}
}
