package com.trb.bdi.service;

import java.util.List;
import java.util.Map;

import com.trb.bdi.vo.ppgVO.PapagoStatsVO;

public interface PapagoStatsService {
	public List<PapagoStatsVO> selectPapagoStats(PapagoStatsVO ps);
	public Map<String,String> insertPapagoStats(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectCredatStats(PapagoStatsVO ps);
}
