package com.trb.bdi.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.trb.bdi.vo.UserInfoVO;

public interface UserInfoService {
	public List<UserInfoVO> selectUserInfo(UserInfoVO ui);
	public List<UserInfoVO> selectUserInfoList(UserInfoVO ui);
	public Map<String,Object> insertUserInfo(UserInfoVO ui);
	public Map<String,Object> updateUserInfo(UserInfoVO ui);
	public Map<String,Object> deleteUserInfo(UserInfoVO ui);
	public Map<String,Object> doLogin(UserInfoVO ui, HttpSession session);
}
