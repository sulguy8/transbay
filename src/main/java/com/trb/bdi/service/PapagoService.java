package com.trb.bdi.service;

import com.trb.bdi.vo.ppgVO.Message;
import com.trb.bdi.vo.ppgVO.TransVO;

public interface PapagoService {
	
	public Message doTranslate(TransVO trs);
}
