package com.trb.bdi.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.trb.bdi.vo.TestInfoVO;
import com.trb.bdi.vo.VenueInfoVO;

public interface VenueInfoService {
	public List<VenueInfoVO> selectVenueInfo(VenueInfoVO vi);
	public List<VenueInfoVO> selectVenueInfoList(VenueInfoVO vi);

	public Map<String,Object> doLogin(VenueInfoVO vi, HttpSession session);
	
	public Map<String,Object> insertVenueInfo(VenueInfoVO vi);
	public Map<String,Object> updateVenueInfo(VenueInfoVO vi);
	public Map<String,Object> deleteVenueInfo(VenueInfoVO vi);

	public Map<String,Object> selectTestInfoList(TestInfoVO test);
}
