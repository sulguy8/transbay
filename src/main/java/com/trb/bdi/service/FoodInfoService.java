package com.trb.bdi.service;

import java.util.List;

import com.trb.bdi.vo.FoodInfoVO;
import com.trb.bdi.vo.UserInfoVO;

public interface FoodInfoService {
	public List<FoodInfoVO> selectFoodInfo(FoodInfoVO fd);
	public List<FoodInfoVO> selectOnkeyList(FoodInfoVO fd);
	
}
