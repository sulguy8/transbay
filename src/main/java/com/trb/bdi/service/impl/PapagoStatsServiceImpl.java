package com.trb.bdi.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.trb.bdi.dao.PapagoStatsDAO;
import com.trb.bdi.service.PapagoStatsService;
import com.trb.bdi.vo.ppgVO.PapagoStatsVO;

@Service
public class PapagoStatsServiceImpl implements PapagoStatsService {
	
	@Resource
	private PapagoStatsDAO psdao;
	@Override
	public List<PapagoStatsVO> selectPapagoStats(PapagoStatsVO ps) {
		return psdao.selectPapagoStats(ps);
	}
	public List<PapagoStatsVO> selectCredatStats(PapagoStatsVO ps) {
		return psdao.selectCredatStats(ps);
	}
	
	@Override
	public Map<String, String> insertPapagoStats(PapagoStatsVO ps) {
		Map<String,String> rMap = new HashMap<String,String>();
		rMap.put("msg", "실패");
		int cnt = psdao.insertPapagoStats(ps);
		if(cnt==1) {
			rMap.put("msg", "성공");
		}
		rMap.put("cnt", cnt+"");
		return rMap;
	}
}
