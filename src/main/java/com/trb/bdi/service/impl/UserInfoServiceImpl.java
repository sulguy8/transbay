package com.trb.bdi.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.trb.bdi.dao.UserInfoDAO;
import com.trb.bdi.service.UserInfoService;
import com.trb.bdi.vo.UserInfoVO;

@Service
public class UserInfoServiceImpl implements UserInfoService {

	@Resource
	private UserInfoDAO uiDao;
	
	@Override
	public List<UserInfoVO> selectUserInfo(UserInfoVO ui) {
		return uiDao.selectUserInfo(ui);
	}
	@Override
	public List<UserInfoVO> selectUserInfoList(UserInfoVO ui) {
		return uiDao.selectUserInfoList(ui);
	}
	@Override
	public Map<String, Object> insertUserInfo(UserInfoVO ui) {
		int cnt = uiDao.insertUserInfo(ui);
		Map<String, Object> rMap = new HashMap<String, Object>();
		rMap.put("msg", "회원가입 실패");
		if(cnt==1) {
			rMap.put("msg", "회원가입 성공");
		}
		return rMap;
	}
	@Override
	public Map<String,Object> updateUserInfo(UserInfoVO ui) {
		int cnt = uiDao.updateUserInfo(ui);
		Map<String, Object> rMap = new HashMap<String, Object>();
		rMap.put("msg", "업데이트 실패");
		if(cnt==1) {
			rMap.put("msg", "업데이트 성공");
		}
		return rMap;
	}
	@Override
	public Map<String, Object> deleteUserInfo(UserInfoVO ui) {
		int cnt = uiDao.deleteUserInfo(ui);
		Map<String, Object> rMap = new HashMap<String, Object>();
		rMap.put("msg", "삭제 실패");
		if(cnt==1) {
			rMap.put("msg", "삭제 성공");
		}
		return rMap;
	}
	
	@Override
	public Map<String, Object> doLogin(UserInfoVO ui, HttpSession session) {
		Map<String,Object> rMap = new HashMap<String, Object>();
		ui = uiDao.doLogin(ui);
		rMap.put("result", "false");
		if(ui!=null) {
			rMap.put("result", "true");
			rMap.put("user", ui);
			session.setAttribute("user", ui.getUiName());
		}
		return rMap;
	}
}
