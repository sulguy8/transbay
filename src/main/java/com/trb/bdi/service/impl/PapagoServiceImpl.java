package com.trb.bdi.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trb.bdi.dao.PapagoInfoDAO;
import com.trb.bdi.dao.PapagoStatsDAO;
import com.trb.bdi.service.PapagoService;
import com.trb.bdi.vo.ppgVO.Message;
import com.trb.bdi.vo.ppgVO.PapagoInfoVO;
import com.trb.bdi.vo.ppgVO.PapagoStatsVO;
import com.trb.bdi.vo.ppgVO.Result;
import com.trb.bdi.vo.ppgVO.TransVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PapagoServiceImpl implements PapagoService {

	private String id = "jLeKZ0mjAYEFUzSroT6s";

	private String secret = "Ll6E5Ql3TY";

	private String apiUrl = "https://openapi.naver.com/v1/papago/n2mt";
	
	@Resource
	private PapagoInfoDAO piDao;
	@Resource
	private PapagoStatsDAO psDao;
	
	private ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	@Override
	public Message doTranslate(TransVO trs) {
		try {
			PapagoStatsVO ps = new PapagoStatsVO();
			ps.setUiNum(trs.getUiNum());
			
			PapagoInfoVO pi = new PapagoInfoVO();
			pi.setPiSource(trs.getSource());
			pi.setPiTarget(trs.getTarget());
			pi.setPiText(trs.getText());
			pi = piDao.selectPapagoInfo(pi);
			
			if(pi!=null) {
				Result r = new Result();
				r.setSrcLangType(pi.getPiSource());
				r.setTarLangType(pi.getPiTarget());
				r.setTranslatedText(pi.getPiResult());
				Message m = new Message();
				m.setResult(r);
				piDao.updatePapagoInfoCnt(pi);
				
				ps.setPiNum(pi.getPiNum());
				psDao.insertPapagoStats(ps);
				return m;
			}
			
			String text = URLEncoder.encode(trs.getText(),"UTF-8");
			
			URL url = new URL(apiUrl);
			HttpURLConnection hc = (HttpURLConnection)url.openConnection();
			hc.setRequestMethod("POST");
			hc.setRequestProperty("content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			hc.setRequestProperty("X-Naver-Client-Id", id);
			hc.setRequestProperty("X-Naver-Client-Secret", secret);
			String param = "source=" +trs.getSource() + "&target=" + trs.getTarget() + "&text=" + text;
			hc.setDoOutput(true);
			DataOutputStream dos = new DataOutputStream(hc.getOutputStream());
			dos.writeBytes(param);
			dos.flush();
			dos.close(); 
			
			InputStreamReader isr = new InputStreamReader(hc.getInputStream(),"UTF-8");
			BufferedReader br = new BufferedReader(isr);
			StringBuffer res = new StringBuffer();
			String str = null;
			while((str=br.readLine())!=null) {
				res.append(str);
			}
				
				TransVO resultTvo = om.readValue(res.toString(), TransVO.class);
				Result r = resultTvo.getMessage().getResult();
				pi = new PapagoInfoVO();
				pi.setPiSource(r.getSrcLangType());
				pi.setPiTarget(r.getTarLangType());
				pi.setPiResult(r.getTranslatedText());
				pi.setPiText(trs.getText());
				
				piDao.insertPapagoInfo(pi);
				ps.setPiNum(pi.getPiNum());
				
				psDao.insertPapagoStats(ps);
				return resultTvo.getMessage();
				
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
