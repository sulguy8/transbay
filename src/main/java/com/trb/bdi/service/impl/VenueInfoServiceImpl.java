package com.trb.bdi.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.trb.bdi.dao.VenueInfoDAO;
import com.trb.bdi.service.VenueInfoService;
import com.trb.bdi.vo.PageVO;
import com.trb.bdi.vo.TestInfoVO;
import com.trb.bdi.vo.VenueInfoVO;

@Service
public class VenueInfoServiceImpl implements VenueInfoService {

	@Resource
	private VenueInfoDAO viDao;
	
	@Override
	public List<VenueInfoVO> selectVenueInfo(VenueInfoVO vi) {
		return viDao.selectVenueInfo(vi);
	}
	@Override
	public List<VenueInfoVO> selectVenueInfoList(VenueInfoVO vi) {
		return viDao.selectVenueInfoList(vi);
	}
	@Override
	public Map<String, Object> doLogin(VenueInfoVO vi, HttpSession session) {
		Map<String,Object> rMap = new HashMap<String, Object>();
//		Venue = uiDao.doLogin(Venue);
//		rMap.put("result", "false");
//		if(Venue!=null) {
//			rMap.put("result", "true");
//			rMap.put("Venue", Venue);
//			session.setAttribute("Venue", Venue.getUiName());
//		}
		return rMap;
	}
	@Override
	public Map<String, Object> insertVenueInfo(VenueInfoVO vi) {
		int cnt = viDao.insertVenueInfo(vi);
		Map<String, Object> rMap = new HashMap<String, Object>();
		rMap.put("msg", "회원가입 실패");
		if(cnt==1) {
			rMap.put("msg", "회원가입 성공");
		}
		return rMap;
	}
	@Override
	public Map<String,Object> updateVenueInfo(VenueInfoVO vi) {
		int cnt = viDao.updateVenueInfo(vi);
		Map<String, Object> rMap = new HashMap<String, Object>();
		rMap.put("msg", "업데이트 실패");
		if(cnt==1) {
			rMap.put("msg", "업데이트 성공");
		}
		return rMap;
	}
	@Override
	public Map<String, Object> deleteVenueInfo(VenueInfoVO vi) {
		int cnt = viDao.deleteVenueInfo(vi);
		Map<String, Object> rMap = new HashMap<String, Object>();
		rMap.put("msg", "삭제 실패");
		if(cnt==1) {
			rMap.put("msg", "삭제 성공");
		}
		return rMap;
	}
	
	@Override
	public Map<String, Object> selectTestInfoList(TestInfoVO test) {
		return viDao.selectTestInfoList(test);
	}
}
