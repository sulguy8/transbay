package com.trb.bdi.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.trb.bdi.dao.FoodInfoDAO;
import com.trb.bdi.service.FoodInfoService;
import com.trb.bdi.vo.FoodInfoVO;

@Service
public class FoodInfoServiceImpl implements FoodInfoService {

	@Resource
	private FoodInfoDAO okdao;
	
	@Override
	public List<FoodInfoVO> selectFoodInfo(FoodInfoVO fd) {
	
		return okdao.selectFoodInfo(fd);
	}

	@Override
	public List<FoodInfoVO> selectOnkeyList(FoodInfoVO fd) {
		return okdao.selectOnKeyList(fd);
	}


}
