    <section class="site-section">
      <div class="container">
        <div class="row">
        <div class="col-lg-3">
        </div>
          <div class="col-lg-6">
            <h2 class="mb-4">Log In To JobBoard</h2>
            <form action="#" class="p-4 border rounded">

              <div class="row form-group">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="text-black" for="fname">ID</label>
                  <input type="text" id="uiId" class="form-control" placeholder="ID">
                </div>
              </div>
              <div class="row form-group mb-4">
                <div class="col-md-12 mb-3 mb-md-0">
                  <label class="text-black" for="fname">Password</label>
                  <input type="password" id="uiPwd" class="form-control" placeholder="Password">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12" align="center">
                  <input type="button" id="loginBtn" value="Log In" class="btn px-4 btn-primary text-white">
                  <input type="button" value="Sign Up" class="btn px-4 btn-primary text-white" data-toggle="modal" data-target="#myModal">
                </div>
              </div>
            </form>
          </div>         
        </div>
      </div>
    </section>
    