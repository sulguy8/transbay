<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <head>
    <title>TransBay</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="Free-Template.co" />
    <link rel="shortcut icon" href="ftco-32x32.png">
    <link rel="stylesheet" href="/resources/bt/app/css/custom-bs.css">
    <link rel="stylesheet" href="/resources/bt/app/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="/resources/bt/app/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bt/app/fonts/icomoon/style.css">
    <link rel="stylesheet" href="/resources/bt/app/fonts/line-icons/style.css">
    <link rel="stylesheet" href="/resources/bt/app/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/resources/bt/app/css/animate.min.css">
    
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="/resources/bt/app/css/style.css">    
    
    <!-- SCRIPTS -->
    <script src="/resources/bt/app/js/jquery.min.js"></script>
    <script src="/resources/bt/app/js/bootstrap.bundle.min.js"></script>
    <script src="/resources/bt/app/js/isotope.pkgd.min.js"></script>
    <script src="/resources/bt/app/js/stickyfill.min.js"></script>
    <script src="/resources/bt/app/js/jquery.fancybox.min.js"></script>
    <script src="/resources/bt/app/js/jquery.easing.1.3.js"></script>
    
    <script src="/resources/bt/app/js/jquery.waypoints.min.js"></script>
    <script src="/resources/bt/app/js/jquery.animateNumber.min.js"></script>
    <script src="/resources/bt/app/js/owl.carousel.min.js"></script>
    
    <script src="/resources/bt/app/js/bootstrap-select.min.js"></script>
    
    <script src="/resources/bt/app/js/custom.js"></script>
  </head>
  
  <body id="top">
  	<div id="overlayer">
  	</div>
  	<div class="loader">
	    <div class="spinner-border text-primary" role="status">
	      <span class="sr-only">Loading...</span>
	    </div>
  	</div>
	<div class="site-wrap">
	    <div class="site-mobile-menu site-navbar-target">
	      <div class="site-mobile-menu-header">
	        <div class="site-mobile-menu-close mt-3">
	          <span class="icon-close2 js-menu-toggle"></span>
	        </div>
	      </div>
	      <div class="site-mobile-menu-body">
	      </div>
	    </div> <!-- .site-mobile-menu -->
    

    <!-- NAVBAR -->
    <header class="site-navbar mt-3">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="site-logo col-6"><a href="index.html">TransBay</a></div>
          <nav class="mx-auto site-navigation">
            <ul class="site-menu js-clone-nav d-none d-xl-block ml-0 pl-0">
              <li><a href="index.html" class="nav-link active">Home</a></li>
              <li><a href="about.html">About***</a></li>
              <li class="has-children">
                <a href="services.html">Pages***</a>
                <ul class="dropdown">
                  <li><a href="services.html">Services***</a></li>
                  <li><a href="service-single.html">Service Single***</a></li>
                  <li><a href="blog-single.html">Blog Single***</a></li>
                  <li><a href="gallery.html">Gallery***</a></li>
                </ul>
              </li>
              <li><a href="blog.html">Blog***</a></li>
              <li><a href="contact.html">Contact***</a></li>
              <li class="d-lg-none"><a href="post-job.html"><span class="mr-2">+</span> Posting***</a></li>
              <li class="d-lg-none"><a id="login" href="/views/main">Log In</a></li>
            </ul>
          </nav>    
          <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
            <div class="ml-auto">
              <a href="post-job.html" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Posting***</a>
              <a id="btLogin" href="/views/main" class="btn btn-primary border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Log In</a>
            </div>
            <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
          </div>
        </div>
      </div>
    </header>

    <!-- HOME -->
    <section class="home-section section-hero overlay bg-image" style="background-image: url('/resources/bt/app/images/main_image.jpg');" id="home-section">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-12">
            <div class="mb-5 text-center">
              <h1 class="text-white font-weight-bold">The Easiest Way To Get EastAsian Foods!</h1>
              <p> Enjoy taste</p>
            </div>
            <form method="post" class="search-jobs-form">
              <div class="row mb-5">
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 mb-4 mb-lg-0">
                  <input type="text" id="text" onkeyup="findId(this)" class="form-control form-control-lg" aria-describedby="helpBlock" value="">
               
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 mb-4 mb-lg-4">
					<select id="target" class="selectpicker" data-style="btn-white btn-lg" data-width="100%" data-live-search="true" title="Select language">
						<option value="ko">Korean</option>
						<option value="ja">Japanese</option>
						<option value="zh-CN">Chinese(Simplified)</option>
						<option value="zh-TW">Chinese(Traditional)</option>
                  	</select>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 mb-4 mb-lg-4">
	                <div class="popular-keywords">
	                 	<h3>Associate search : </h3><br>
	       				<ul id="ulId" class="keywords list-unstyled m-0 p-0"></ul>
                	</div>
                </div>    
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 mb-4 mb-lg-4">
                  <a href="#next" id="btn" class="btn btn-primary btn-lg btn-block text-white btn-search"><span class="icon-search icon mr-2"> Search Food!</span></a>
                </div>
              </div>
            </form>
          </div>
	  </div>
      </div>
    </section>
    
    <!-- Content -->
    <section class="py-5 bg-image overlay-primary fixed overlay bckimg" id="next" style="background-image: url('/resources/bt/app/images/main_image.jpg');">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-7 text-center">
            <h2 class="section-title mb-2 text-white centerTheme"></h2>
            <p class="lead text-white fdInfo"></p>
          </div>
        </div>
        <div class="row pb-0 block__19738 section-counter">
          <div class="col-6 col-md-6 col-lg-3 mb-5 mb-lg-0">
            <div class="d-flex align-items-center justify-content-center mb-2">
              <strong class="number fdCnt"></strong>
            </div>
            <span class="caption">Search Times</span>
          </div>
          <div class="col-6 col-md-6 col-lg-3 mb-5 mb-lg-0">
            <div class="d-flex align-items-center justify-content-center mb-2">
              <strong class="number fdCstm"></strong>
            </div>
            <span class="caption">Country</span>
          </div>
          <div class="col-6 col-md-6 col-lg-3 mb-5 mb-lg-0">
            <div class="d-flex align-items-center justify-content-center mb-2">
              <strong class="number fdStore"></strong>
            </div>
            <span class="caption">Restaurant</span>
          </div>
          <div class="col-6 col-md-6 col-lg-3 mb-5 mb-lg-0">
            <div class="d-flex align-items-center justify-content-center mb-2">
              <strong class="number fdLike"></strong>
            </div>
            <span class="caption">Like it!</span>
          </div>
        </div>
      </div>
    </section>

    <section id="vnContent" class="site-section">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-7 text-center">
            <h2 id="venueTitle" class="section-title mb-2"></h2>
          </div>
        </div>
		<div class="row mb-5">
    	    <div class="col-12 col-sm-6 col-md-6 col-lg-6 mb-4 mb-lg-0">
            	<input type="text" id="sortName" class="form-control form-control-lg" aria-describedby="helpBlock" value="">
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-6 mb-4 mb-lg-4">
				<select id="sortFood" class="selectpicker" data-style="btn-white btn-lg" data-width="100%" data-live-search="true" title="Select Count">
					<option value="Korea Food">Korea Food</option>
					<option value="Japan Food">Japan Food</option>
					<option value="China Food">China Food</option>
               	</select>
            </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 mb-4 mb-lg-4">
                  <a href="#vnContent" id="srchRstnt" class="btn btn-primary btn-lg btn-block text-white btn-search"><span class="icon-search icon mr-2"> Search Restaurant!</span></a>
                </div>
		</div>        
        <ul id="venueList" class="job-listings mb-5">
          
        </ul>
        <div class="row pagination-wrap">
          <div class="col-md-6 text-center text-md-left mb-4 mb-md-0">
            <span></span>
          </div>
          <div class="col-md-6 text-center text-md-right">
            <div id="pageList" class="custom-pagination ml-auto">
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <!-- Footer -->
    <footer class="site-footer">
      <a href="#home-section" class="smoothscroll scroll-top">
        <span class="icon-keyboard_arrow_up"></span>
      </a>
      <div class="container">
        <div class="row mb-5">
          <div class="col-6 col-md-3 mb-4 mb-md-0">
            <h3>Search Trending***</h3>
            <ul class="list-unstyled">
            </ul>
          </div>
          <div class="col-6 col-md-3 mb-4 mb-md-0">
            <h3>Company***</h3>
            <ul class="list-unstyled">
              <li><a href="#">About Us***</a></li>
              <li><a href="#">Blog***</a></li>
              <li><a href="#">Resources***</a></li>
            </ul>
          </div>
          <div class="col-6 col-md-3 mb-4 mb-md-0">
            <h3>Support***</h3>
            <ul class="list-unstyled">
              <li><a href="#">Support***</a></li>
              <li><a href="#">Privacy***</a></li>
              <li><a href="#">Terms of Service***</a></li>
            </ul>
          </div>
          <div class="col-6 col-md-3 mb-4 mb-md-0">
            <h3>Contact Us***</h3>
            <div class="footer-social">
              <a href="#"><span class="icon-facebook"></span></a>
              <a href="#"><span class="icon-twitter"></span></a>
              <a href="#"><span class="icon-instagram"></span></a>
              <a href="#"><span class="icon-linkedin"></span></a>
            </div>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-12">
            <p class="copyright"><small>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></small></p>
          </div>
        </div>
      </div>
    </footer>
  </div>
  
  	<!-- View Script -->
	<script>
	window.onload=function(){
		var sessionId = '<%=session.getAttribute("user")%>';
		if (sessionId == 'null'){
		} else if(sessionId != null){
			$('#login').text('Logout');
			$('#login').attr('href','/user/logout');
			$('#btLogin').text('Logout');
			$('#btLogin').attr('href','/user/logout');		
		}  
		$('#bgList').on('click',function(){
			if(sessionId == 'null') {
				alert('You need Login');
			} else {
			}
		});
	}  
	
	$(document).ready(function(){
		$.ajax({
			url:'/venue',
			method:'GET',
			success:function(res){
				venues(res);
				$('#srchRstnt').on('click',function(){	
					$.ajax({
						url:'/venue',
						method:'GET',
						data:'vnName=' + $('#sortName').val() + '&vnCtry=' + $('#sortFood').val(),
						success:function(res){
							venues(res);
						},
						error:function(res){
							console.log(res);
						}
					});						
				}) 
			},
			error:function(res){
				console.log(res);
			}
		});
	});
  	</script>
  	
  	<!-- Click Event Script -->
  	<script>
	$(document).ready(function(){
		$('#btn').on('click',function(){
			var param = 'source=en' + '&target=' + $('#target').val() + '&text=' + $('#text').val();
			$.ajax({
				url:'/papago',
				method:'POST',
				data:param,
				beforeSend: function(xhr){
					xhr.setRequestHeader('x-ajax', 'true');
				},
				success:function(res){
					var transText = res.result.translatedText;
	 					$.ajax({
							url:'/food',
							method:'GET',
							data: 'fdName=' + $('#text').val(),
							success:function(res){	
								$('.centerTheme').text(transText);
								$('.fdInfo').text(res[0].fdName);
					            $('.fdCnt').text(res[0].fdCnt);
					            $('.fdCstm').text(res[0].fdCstm);
					            $('.fdStore').text(res[0].fdStore);
					            $('.fdLike').text(res[0].fdLike);
					            $('.bckimg').attr('style','background-image:url(\'https://' + res[0].fdImg + '\')');
							},
							error:function(res){
								console.log(res);
							}
						});	 
				},
				error:function(res){
					console.log(res);
				}
			});
		})
	});
	
	$(document).on("click", ".scroll-down", function(e){ 
 		var str = $(e.target).text().trim();
 		var param = 'source=en' + '&target=ko' + '&text=' + str;
		$.ajax({
			url:'/papago',
			method:'POST',
			data:param,
			beforeSend: function(xhr){
				xhr.setRequestHeader('x-ajax', 'true');
			},
			success:function(res){
				var transText = res.result.translatedText;
 					$.ajax({
						url:'/food',
						method:'GET',
						data: 'fdName=' + str,
						success:function(res){
							$('.centerTheme').text(transText);
							$('.fdInfo').text(str);
				            $('.fdCnt').text(res[0].fdCnt);
				            $('.fdCstm').text(res[0].fdCountry);
				            $('.fdStore').text(res[0].fdStore);
				            $('.fdLike').text(res[0].fdLike);
				          
						},
						error:function(res){
							console.log(res);
						}
					});	 
			},
			error:function(res){
				console.log(res);
			}	
		});
	});	
	</script>
	
	<!-- Function Script -->
	<script>
	function findId(f){
		var xhr = new XMLHttpRequest();
		var url = '/onkey/text?';
		if(f.value.trim().length>0){
			url += 'id=' +f.value.trim();
		};
		xhr.open('GET',url);
		xhr.onreadystatechange = function(){
			if(xhr.readyState==xhr.DONE){
				var html = '';
				if(xhr.status==200){
					var num;
					var idList = JSON.parse(xhr.responseText);
					for(var i=0; i<idList.result.length; i++){
						html += '<li><a href="#next" class="smoothscroll scroll-down">' + idList.result[i].fdName + '</a></li>&nbsp;';	
						if(i > 1){
							break;
						} 
					}
				}			
				document.querySelector('#ulId').innerHTML = html;
			}
		}
		xhr.send();
	}
	
	function venues(res){
		$('#venueTitle').text(res.length + ' Restaurant Listed'); 
		var sessionId = '<%=session.getAttribute("user")%>';
		var html = '';
		for(var i=0; i<res.length; i++){
			html += '<li class="job-listing d-block d-sm-flex pb-3 pb-sm-0 align-items-center">';
				if(sessionId == 'null') {
					html += '<a id="bgList" href="/views/main"></a>';
				} else {
					html += '<a id="bgList" href="/views/blog"></a>';
				}		
			html += '<div class="job-listing-logo">';
			html += '<img src="https://' + res[i].vnImg + '" alt="Free Website Template by Free-Template.co" class="img-fluid">';
			html += '</div><br>';		
			html += '<div class="job-listing-about d-sm-flex custom-width w-100 justify-content-between mx-4">';
			html += '<div class="job-listing-position custom-width w-50 mb-3 mb-sm-0">';
			html += '<h2>' + res[i].vnName + '</h2>';
			html += '<strong>' + res[i].vnCtry + '</strong>';
			html += '</div>'; 		
			html += '<div class="job-listing-location mb-3 mb-sm-0 custom-width w-25">';
			html += '<span class="icon-room"></span>' + res[i].vnAddr;
			html += '</div>';		
			html += '<div class="job-listing-meta">';
			html += '<span class="badge badge-danger">' + res[i].vnLike + ' like!</span>';
			html += '</div>'; 	
			html += '</div>';	
			html += '</li>';		
			if(i == 10){
				break;
			} 
		}
		document.querySelector('#venueList').innerHTML = html;
	}
</script> 
</body>
</html>