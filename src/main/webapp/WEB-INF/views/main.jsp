<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>TransBay</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/resources/bt/app/css/custom-bs.css">
    <link rel="stylesheet" href="/resources/bt/app/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="/resources/bt/app/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/resources/bt/app/fonts/icomoon/style.css">
    <link rel="stylesheet" href="/resources/bt/app/fonts/line-icons/style.css">
    <link rel="stylesheet" href="/resources/bt/app/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/resources/bt/app/css/animate.min.css">
    <link rel="stylesheet" href="/resources/bt/app/css/quill.snow.css">
    
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="/resources/bt/app/css/style.css">   
    
    <!-- SCRIPTS -->
    <script src="/resources/bt/app/js/jquery.min.js"></script>
    <script src="/resources/bt/app/js/bootstrap.bundle.min.js"></script>
    <script src="/resources/bt/app/js/isotope.pkgd.min.js"></script>
    <script src="/resources/bt/app/js/stickyfill.min.js"></script>
    <script src="/resources/bt/app/js/jquery.fancybox.min.js"></script>
    <script src="/resources/bt/app/js/jquery.easing.1.3.js"></script>
    <script src="/resources/bt/app/js/jquery.waypoints.min.js"></script>
    <script src="/resources/bt/app/js/jquery.animateNumber.min.js"></script>
    <script src="/resources/bt/app/js/owl.carousel.min.js"></script>
    <script src="/resources/bt/app/js/bootstrap-select.min.js"></script>
    <script src="/resources/bt/app/js/custom.js"></script> 
  </head>
  <body id="top">
  	<div id="overlayer"></div>
  	<div class="loader">
    	<div class="spinner-border text-primary" role="status">
      	<span class="sr-only">Loading...</span>
    	</div>
  	</div>
	<div class="site-wrap">
    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
	<!-- NAVBAR -->
    <header class="site-navbar mt-3">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="site-logo col-6"><a href="/views/index">TransBay</a></div>
          <nav class="mx-auto site-navigation">
            <ul class="site-menu js-clone-nav d-none d-xl-block ml-0 pl-0">
              <li><a href="index.html" class="nav-link active">Home</a></li>
              <li><a href="about.html">About***</a></li>
              <li class="has-children">
                <a href="services.html">Pages***</a>
                <ul class="dropdown">
                  <li><a href="services.html">Services***</a></li>
                  <li><a href="service-single.html">Service Single***</a></li>
                  <li><a href="blog-single.html">Blog Single***</a></li>
                  <li><a href="gallery.html">Gallery***</a></li>
                </ul>
              </li>
              <li><a href="blog.html">Blog***</a></li>
              <li><a href="contact.html">Contact***</a></li>
              <li class="d-lg-none"><a href="post-job.html"><span class="mr-2">+</span> Posting***</a></li>
              <li class="d-lg-none"><a id="login" href="/views/main">Log In</a></li>
            </ul>
          </nav>    
          <div class="right-cta-menu text-right d-flex aligin-items-center col-6">
            <div class="ml-auto">
              <a href="post-job.html" class="btn btn-outline-white border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-add"></span>Posting***</a>
              <a id="btLogin" href="/views/main" class="btn btn-primary border-width-2 d-none d-lg-inline-block"><span class="mr-2 icon-lock_outline"></span>Log In</a>
            </div>
            <a href="#" class="site-menu-toggle js-menu-toggle d-inline-block d-xl-none mt-lg-2 ml-3"><span class="icon-menu h3 m-0 p-0 mt-2"></span></a>
          </div>
        </div>
      </div>
    </header>

    <!-- HOME -->
    <section class="section-hero overlay inner-page bg-image" style="background-image: url('/resources/bt/app/images/main_image.jpg');" id="home-section">
      <div class="container">
        <div class="row">
          <div class="col-md-7">
            <h1 class="text-white font-weight-bold">Sign Up/Login</h1>
            <div class="custom-breadcrumbs">
              <a href="#">Home</a> <span class="mx-2 slash">/</span>
              <span class="text-white"><strong>Log In</strong></span>
            </div>
          </div>
        </div>
      </div>
    </section>
    
	<!-- Content -->
	<jsp:include page="login.jsp" flush="false" />
    
    
	<!-- Footer -->
    <footer class="site-footer">
      <a href="#home-section" class="smoothscroll scroll-top">
        <span class="icon-keyboard_arrow_up"></span>
      </a>
      <div class="container">
        <div class="row mb-5">
          <div class="col-6 col-md-3 mb-4 mb-md-0">
            <h3>Search Trending***</h3>
            <ul class="list-unstyled">
            </ul>
          </div>
          <div class="col-6 col-md-3 mb-4 mb-md-0">
            <h3>Company***</h3>
            <ul class="list-unstyled">
              <li><a href="#">About Us***</a></li>
              <li><a href="#">Blog***</a></li>
              <li><a href="#">Resources***</a></li>
            </ul>
          </div>
          <div class="col-6 col-md-3 mb-4 mb-md-0">
            <h3>Support***</h3>
            <ul class="list-unstyled">
              <li><a href="#">Support***</a></li>
              <li><a href="#">Privacy***</a></li>
              <li><a href="#">Terms of Service***</a></li>
            </ul>
          </div>
          <div class="col-6 col-md-3 mb-4 mb-md-0">
            <h3>Contact Us***</h3>
            <div class="footer-social">
              <a href="#"><span class="icon-facebook"></span></a>
              <a href="#"><span class="icon-twitter"></span></a>
              <a href="#"><span class="icon-instagram"></span></a>
              <a href="#"><span class="icon-linkedin"></span></a>
            </div>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-12">
            <p class="copyright"><small>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></small></p>
          </div>
        </div>
      </div>
    </footer>
  </div>
  
   	<!-- MainPage, Login, Logout Script -->
	<script>
	window.onload=function(){
		var sessionId = '<%=session.getAttribute("user")%>';
		if (sessionId == 'null'){
			
		} else if(sessionId != null){
			$('#ssName').text('Welcome! ' + sessionId); 
		/* 	location.href = '/views/index'; */
		}  
	}
	
	$(document).ready(function(){
		$('#loginBtn').on('click',function(){
			var uiId = document.querySelector('#uiId');
			if(uiId.value.trim().length<5){
				alert('ID must be at least 5 characters');
				uiId.value='';
				uiId.focus();
				return false;
			}
			var uiPwd = document.querySelector('#uiPwd');
			if(uiPwd.value.trim().length<5){
				alert('Password must be at least 5 characters');
				uiPwd.value='';
				uiPwd.focus();
				return false;
			}
			var param = {
					uiId:$('#uiId').val(),
					uiPwd:$('#uiPwd').val()
			}
			param = JSON.stringify(param);
			$.ajax({
				url:'/user/login',
				method:'POST',
				data:param,
				beforeSend:function(xhr){
				},
				contentType:'application/json',
				success:function(res){
					if(res.result == 'true'){
						alert('Welcome! '+ res.user.uiName);	
						location.href = '/views/index';
					} else if(res.result == 'false'){
						alert('Please check your ID and password.');
					}
				},
				error:function(res){
					console.log(res);
					alert(res.msg);
					
				} 
			})
		})
	})
	</script>
     
	<!-- Modal -->
	<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	      	<h4 class="modal-title" id="myModalLabel" align="left">Sign Up</h4>
	      	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>  
	      </div>
	      <div class="modal-body">
	               <div class="form-group">
		                <label for="usrId" style="float:left;">* ID </label><button type="button" class="btn btn-primary btn-circled" onclick="valCkId()" style="float:right;">&nbsp;check&nbsp;</button>  
		                <input type="text" id="usrId" name="usrId" class="form-control" placeholder="" data-title="ID" maxlength="16"/>
						          
	                </div> 
	
	                <div class="form-group">
	                    <label for="usrPwd">* Password </label>
	                    <input type="password" id="usrPwd" name="usrPwd" class="form-control" placeholder="" data-title="Password" maxlength="29"/>
	                </div>  
	                
	                <div class="form-group">
	                    <label for="usrPwdCk">* Password Check</label>
	                    <input type="password" id="usrPwdCk" name="usrPwdCk" class="form-control" placeholder="" data-title="Password Check" maxlength="29"/>
	                </div>
	                         
	                <div class="form-group">
	                    <label for="usrName">* Name</label>
	                    <input type="text" id="usrName" name="usrName" class="form-control" placeholder="" data-title="Name" maxlength="29"/>
	                </div>
	                
	                <div class="form-group">
	                    <label for="usrTel" style="float:left;">* Contact</label>
	                    <input type="text" class="form-control" id="usrTel" name="usrTel" placeholder="" data-title="Contact" maxlength="29"/>
	                </div>
	                <div class="form-group">
	                    <label for="usrEmail" style="float:left;">* Email Address</label>
	                    <input type="text" class="form-control" id="usrEmail" data-title="Email Address" placeholder="">
	                </div>                
	                <div class="form-group">
	                    <div class="box_scroll">
	                        <strong class="terms_tit">Consent to Collection and Use of Personal Information</strong>
	                            <pre class= "pre-scrollable">
1. Collection and use items: name, phone number, email address<br>
2. Purpose of collection and use: Confirmation of subscription inquiries and subscription details<br>
3. Retention period: 1 year from receipt of inquiry<br>
If you do not agree, you will not be able to join.
	                            </pre>                                  
	                    </div>
	                	<input type="checkbox" id="agreeY" />  I agree.    
				          
	                </div>            
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <div style="float: right;"><button type="button" class="btn btn-primary btn-circled" data-dismiss="" onclick="javascript:signUp.insert();" style="font-size:15px;">Submit</button></div>
	      </div>
	    </div>
	  </div>
	</div>
	
	<!-- Validation Check Script -->
	<script>
	var signUp = {
		    insert: function test(){
		        if(signUp.validate.check()){
		            var email = $("#usrEmail").val();
		            var telChk = $('#usrTel').val();
		            $('.submit').attr('data-dismiss','');
		           	// 이메일은 @, .~~ 포함되어있어야 된다.
		       		// 전화번호는 00000000000 or 000-0000-0000로 구성되어야 한다.  
		            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		            var ev = /^[0-9]{2,3}[-]+[0-9]{3,4}[-]+[0-9]{4}$/
		            var nev = /^[0-9]{2,3}[0-9]{3,4}[0-9]{4}$/
		            
		            if($("#usrPwd").val() != $("#usrPwdCk").val()){
		                alert("Passwords do not match.");
		                $("#usrPwdCk").focus();
		           		return false;
		            }
	
		            if(!ev.test(telChk)){
			            if (!nev.test(telChk)) {
			            	alert("Enter a valid phone number.");
			            	$("#usrTel").focus();
			            	return false;
			            }
		            }
		            
		            if(!re.test(email)) {
		                alert("Enter a valid email address.");
		                $("#usrEmail").focus();
		                return false;
		            }
		            
		            if($("#agreeY").prop("checked") ) { 
	
		            }else {
		                alert("You must agree to the terms of collection and use of personal information.");
		                $("#agreeY").first().focus();
		                return false;
		            }
		                     
		            var param = {
		     				uiId : $('#usrId').val().trim(),
		     				uiPwd : $('#usrPwd').val().trim(),
		     				uiName : $('#usrName').val().trim(),
		     				uiEmail : $('#usrEmail').val().trim(),
		     				uiTel : $('#usrTel').val().trim()
		     		}
		            
		     		$.ajax({
		     			url:'/user',
		     			method:'POST',
		     			data:JSON.stringify(param),
		    			beforeSend : function(xhr){
		    				xhr.setRequestHeader('Content-type','application/json;charset=utf-8')
		    			},
		     			success:function(res){
		     				alert(res.msg);
		     			}, 
		     			error:function(res){
		     				alert('Membership failed.');
		     			}
		     		})
		            $('.btn-circled').attr('data-dismiss','modal');
		        }
		    },
		    validate: {
		        check: function(){
		            if($("[name=usrId]").val() == ''){
		            	signUp.validate.warning($("[name=usrId]"));
		                return false;
		            } else if($("#usrId").val().length<5){
						alert('ID must be at least 5 characters.');
						$("#usrId").focus();
						return false;
					}
		            if($("#usrPwd").val() == ''){
		            	signUp.validate.warning($("#usrPwd"));
		                return false;
		            } else if($("#usrPwd").val().length<5){
						alert('Password must be at least 5 characters.');
						$("#usrPwd").focus();
						return false;
					}
		            
		            if($("#usrPwdCk").val() == ''){
		                alert('Check your password');
		                return false;
		            }
		            
		            if($("#usrName").val() == ''){
		            	signUp.validate.warning($("#usrName"));
		                return false;
		            }
		            if($("#usrTel").val() == ''){
		            	signUp.validate.warning($("#usrTel"));
		                return false;
		            }
		            if($("#usrEmail").val() == ''){
		            	signUp.validate.warning($("#usrEmail"));
		                return false;
		            }
		            
		            return true;
		        }, 
		        warning: function(obj){
		            $('.submit').attr('data-dismiss','');
		            alert('Please enter ' + obj.data('title'));
		            obj.focus();
		        },
		    },
		};
	
		function valCkId() {
			var id = $('#usrId').val().trim();
			if($('#usrId').val().trim()==''){
				alert('Please enter your ID.');
			} else{
				$.ajax({
			 		url:'/user',
			 		method:'GET',
			 		data: 'uiId=' + $('#usrId').val().trim(),
			 		success:function(res){
			 			if(res.uiId != null) {
			 				alert('This ID is already taken.'); 			
			 			} else{
			 				alert('ID is available.');
			 				$('#usrId').val(id);
			 			}
			 		}, 
			 		error:function(res){
			 			alert('Membership failed.');
			 		}
		 		})
			}
		}
	</script>
  </body>
</html>